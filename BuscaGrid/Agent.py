

################################################################
#                                                              #
#                                                              #
#      Algoritmos de Busca no GRID - Sistemas Inteligentes     #
#                                                              #
#                                                              #
#      Equipe:   Steven Justen Tunnicliff                      #
#                Vinícius Sandri Diaz                          #
#                                                              #
#                                                              #
#                                                              #
#      data da última modificação: 04/09/18                    #
#                                                              #
#      Implementação:    Agent                                 #
#                                                              #
#                                                              #
#               Autor: Vinícius Sandri Diaz                    #
#                                                              #
#                                                              #
#                                                              #
################################################################

from Tree import *
import math


class Position:

    mine_position = []
    mine_angle = 0


class Agent:

    def __init__(self):
        self.goals = []
        self.line_size = 0
        self.column_size = 0
        self.know_environment = []
        self.action = []
        self.position = Position

    # Funções sets e voids
    def buildAgent(self, t_angle, t_agent_position, t_environment):

        self.position.mine_angle = t_angle
        self.position.mine_position = t_agent_position
        self.know_environment = t_environment
        self.line_size = len(t_environment)
        self.column_size = len(t_environment)


    def setGoals(self, t_goal):
        self.goals = t_goal

    def runCommands(self):
        while self.actions:
            command = self.commands.pop(0)
            if command in self.actions():
                self.moveAgent(command)

    # Funçoes gets
    def getPositions(self):
        return self.position.mine_position

    def actions(self,  t_angle, t_position):
        # retorna [ angulo, [linha, coluna] ]
        action = []

        # Para angulo atual de 0

        if (t_position[1] + 1) < self.column_size and t_angle == 0:
            if self.know_environment[t_position[0]][t_position[1] + 1] != 1:
                action.append([0, [t_position[0], t_position[1] + 1]])

        if (t_position[0] - 1) >= 0 and t_angle == 0:
            if self.know_environment[t_position[0] - 1][t_position[1]] != 1:
                action.append([90, [t_position[0] - 1, t_position[1]]])

        if (t_position[0] + 1) < self.line_size and t_angle == 0:
            if self.know_environment[t_position[0] + 1][t_position[1]] != 1:
                action.append([270, [t_position[0] + 1, t_position[1]]])

        # Para angulo atual de 90

        if (t_position[0] - 1) >= 0 and t_angle == 90:
            if self.know_environment[t_position[0] - 1][t_position[1]] != 1:
                action.append([90, [t_position[0] - 1, t_position[1]]])

        if (t_position[1] - 1) >= 0 and t_angle == 90:
            if self.know_environment[t_position[0]][t_position[1] - 1] != 1:
                action.append([180, [t_position[0], t_position[1] - 1]])

        if (t_position[1] + 1) < self.column_size and t_angle == 90:
            if self.know_environment[t_position[0]][t_position[1] + 1] != 1:
                action.append([0, [t_position[0], t_position[1] + 1]])

        # Para angulo atual de 180

        if (t_position[1] - 1) >= 0 and t_angle == 180:
            if self.know_environment[t_position[0]][t_position[1] - 1] != 1:
                action.append([180, [t_position[0], t_position[1] - 1]])

        if (t_position[0] + 1) < self.line_size and t_angle == 180:
            if self.know_environment[t_position[0] + 1][t_position[1]] != 1:
                action.append([270, [t_position[0] + 1, t_position[1]]])

        if (t_position[0] - 1) >= 0 and t_angle == 180:
            if self.know_environment[t_position[0] - 1][t_position[1]] != 1:
                action.append([90, [t_position[0] - 1, t_position[1]]])

        # Para angulo atual de 270

        if (t_position[0] + 1) < self.line_size and t_angle == 270:
            if self.know_environment[t_position[0] + 1][t_position[1]] != 1:
                action.append([270, [t_position[0] + 1, t_position[1]]])

        if (t_position[1] - 1) >= 0 and t_angle == 270:
            if self.know_environment[t_position[0]][t_position[1] - 1] != 1:
                action.append([180, [t_position[0], t_position[1] - 1]])

        if (t_position[1] + 1) < self.column_size and t_angle == 270:
            if self.know_environment[t_position[0]][t_position[1] + 1] != 1:
                action.append([90, [t_position[0], t_position[1] + 1]])

        return action

    def moveAgent(self, t_position):

        # Para não estrapolar o espaço do grid e para verificar
        if 0 <= t_position[0] - 1 < self.line_size and 0 <= t_position[1] - 1 < self.column_size:

            if self.know_environment[t_position[0]][t_position[1] - 1] != 1:
                self.position.mine_angle = 180
                self.position.mine_position = [t_position[0], t_position[1] - 1]

            if self.know_environment[t_position[0]+1][t_position[1]] != 1:
                self.position.mine_angle = 90
                self.position.mine_position = [[t_position[0], t_position[1]]]

            if self.know_environment[t_position[0]][t_position[1] + 1] != 1:
                self.position.mine_angle = 0
                self.position.mine_position = [[t_position[0], t_position[1] + 1]]

    # seção dos algoritmos de ia

    def breadthFirstSearch(self):

        # Adaptado de: https://gist.github.com/thbighead/2e38b9b7df7054ae7d6389d090f50aa1

        queue_solution = []
        tree = Tree()
        node = Node([], [],  self.position.mine_angle, self.position.mine_position[:])
        tree.insertNodes(node)
        tree.insertBorder(node)

        while tree.border:
            node = tree.border.pop(0)
            possibility = self.actions(node.action, node.position)

            for action in possibility:
                node_tmp = Node(node, [], action[0], action[1])

                if node_tmp.position not in tree.visited and not tree.searchTreeBorder(node_tmp.position):
                    tree.insertNodes(node_tmp)
                    tree.insertBorder(node_tmp)
                    tree.visited.append(node_tmp.position)

                    if node_tmp.position == self.goals:

                        while node_tmp.father:
                            queue_solution.append([node_tmp.action, node_tmp.position])
                            node_tmp = node_tmp.father
                        queue_solution.reverse()
                        break

        print("nodes in the tree: " + str(len(tree.node)))
        print("node explored: " + str(len(tree.visited)))
        return queue_solution

    def aStar(self):

        # Adaptado de: https://gist.github.com/jamiees2/5531924

        queue_solution = []
        tree = Tree()
        node = Node([], [], self.position.mine_angle, self.position.mine_position[:])
        tree.insertNodes(node)
        tree.insertBorder(node)

        while tree.border:

            tree.sortBorder()
            node = tree.border.pop(0)
            tree.visited.append(node.position)

            if node.position == self.goals:

                while node.father:
                    queue_solution.append([node.action, node.position])
                    node = node.father
                queue_solution.reverse()
                break

            possibility = self.actions(node.action, node.position)
            for action in possibility:

                # Para calcular a distancoa euclediana
                assessing = math.sqrt(
                    abs(action[1][0] - self.goals[0]) ** 2 + abs(action[1][1] - self.goals[1]) ** 2)

                node_tmp = Node(node, [], action[0], action[1])
                node_tmp.setCost(node.cost, assessing)

                weighted_flag = False
                if node_tmp.position not in tree.visited:
                    for visited in tree.border:
                        if node_tmp.position == visited.position:
                            if node_tmp.weighted > visited.weighted:
                                weighted_flag = True

                    if not weighted_flag:
                        tree.insertNodes(node_tmp)
                        tree.insertBorder(node_tmp)

        print("nodes in the tree: " + str(len(tree.node)))
        print("node explored: " + str(len(tree.visited)))
        return queue_solution
