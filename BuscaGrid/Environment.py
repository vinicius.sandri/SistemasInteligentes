
################################################################
#                                                              #
#                                                              #
#      Algoritmos de Busca no GRID - Sistemas Inteligentes     #
#                                                              #
#                                                              #
#      Equipe:   Steven Justen Tunnicliff                      #
#                Vinícius Sandri Diaz                          #
#                                                              #
#                                                              #
#                                                              #
#      data da última modificação: 27/08/18                    #
#                                                              #
#      Implementação:    Environment                           #
#                                                              #
#                                                              #
#               Autor: Vinícius Sandri Diaz                    #
#                                                              #
#                                                              #
#                                                              #
################################################################


import numpy as np


class Environment(object):

    def __init__(self):

        self.matrix_grid = np

    def buildMatrix(self, t_lines, t_columns):

        self.matrix_grid = np.zeros(shape=(t_lines, t_columns))

    def addWall(self, t_index_line, t_index_column):

        tmp = self.matrix_grid.shape

        if t_index_line <= tmp[0] and t_index_column <= tmp[1]:
            self.matrix_grid[t_index_line, t_index_column] = 1

    def addPosition(self, t_index_line, t_index_column, simbol):

        tmp = self.matrix_grid.shape

        if t_index_line <= tmp[0] and t_index_column <= tmp[1]:
            if self.matrix_grid[t_index_line, t_index_column] != 1:
                self.matrix_grid[t_index_line, t_index_column] = simbol

    def addwallDefault(self):

        # Linha 4
        self.matrix_grid[4, 4] = 1
        self.matrix_grid[4, 5] = 1
        self.matrix_grid[4, 6] = 1
        self.matrix_grid[4, 7] = 1
        self.matrix_grid[4, 8] = 1
        self.matrix_grid[4, 9] = 1

        # Linha 5
        self.matrix_grid[5, 4] = 1
        self.matrix_grid[5, 5] = 1
        self.matrix_grid[5, 6] = 1
        self.matrix_grid[5, 7] = 1
        self.matrix_grid[5, 8] = 1
        self.matrix_grid[5, 9] = 1

    def printMatrix(self):

        print(self.matrix_grid)

    def getMatrixGrid(self):

        return self.matrix_grid
